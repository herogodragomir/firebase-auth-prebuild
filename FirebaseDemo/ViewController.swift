//
//  ViewController.swift
//  FirebaseDemo
//
//  Created by Edy Cu Tjong on 6/16/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseUI

class ViewController: UIViewController, FUIAuthDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func loginPressed(_ sender: Any) {
        guard let authUI = FUIAuth.defaultAuthUI() else { return }
        authUI.delegate = self
        authUI.providers = [FUIEmailAuth()]
        self.present(authUI.authViewController(), animated: true)
    }
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        guard error == nil else {
            return
        }
        self.performSegue(withIdentifier: "goHome", sender: self)
    }
}

